import java.util.Scanner;

public class AB1A1T1ZweiGleicheZahlen {

	public static void main(String[] args) {
		
		int zahl1;
		int zahl2;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie die erste Zahl ein:");
		zahl1 = scn.nextInt();
		
		System.out.println("Geben Sie die zweite Zahl ein:");
		zahl2 = scn.nextInt();
		
		scn.close();
		
		if (zahl1 == zahl2) {
			System.out.println("Sie haben zweimal die selbe Zahl gew�hlt.");
		}
	}

}
