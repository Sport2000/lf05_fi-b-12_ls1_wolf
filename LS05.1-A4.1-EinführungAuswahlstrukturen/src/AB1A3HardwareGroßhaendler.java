import java.util.Scanner;

public class AB1A3HardwareGro�haendler {

	public static void main(String[] args) {
		
		int mausAnzahl;
		double mausPreis;
		double mwst = 0.19;
		int lieferkosten = 10;
		double rechnungsbetrag;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Wie viele M�use wurden bestellt?");
		mausAnzahl = scn.nextInt();
		
		System.out.println("Wie lautet der Einzelpreis?");
		mausPreis = scn.nextDouble();
		
		scn.close();
		
		if (mausAnzahl < 10) {
			rechnungsbetrag = mausAnzahl * mausPreis + lieferkosten + ((mausAnzahl * mausPreis + lieferkosten) * mwst);
		} else {
			rechnungsbetrag = mausAnzahl * mausPreis + ((mausAnzahl * mausPreis) * mwst);
		}
		
		System.out.println("Der Rechnungsbetrag incl. MwSt lautet: " + rechnungsbetrag + " Euro");
	}

}
