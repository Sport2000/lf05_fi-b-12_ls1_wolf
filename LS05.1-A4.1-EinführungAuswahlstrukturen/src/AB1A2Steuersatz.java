import java.util.Scanner;

public class AB1A2Steuersatz {

	public static void main(String[] args) {
		
		double nettowert;
		double steuersatzVoll = 0.19;
		double steuersatzErmaeßigt = 0.07;
		double bruttobetrag = 0;
		String steuersatzWahl;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben sie den Nettowert ein: ");
		nettowert = scn.nextDouble();
		
		System.out.println("Soll der ermeaßigte Steuersatz verwendet werden? (j/n)");
		steuersatzWahl = scn.next();
		
		scn.close();
		
		if (steuersatzWahl.equals("j")) {
			bruttobetrag = nettowert - (nettowert * steuersatzErmaeßigt);
		} else if (steuersatzWahl.equals("n")) {
			bruttobetrag = nettowert - (nettowert * steuersatzVoll);
		}
		
		System.out.println ("Der Bruttobetrag lautet: " + bruttobetrag);
	}

}
