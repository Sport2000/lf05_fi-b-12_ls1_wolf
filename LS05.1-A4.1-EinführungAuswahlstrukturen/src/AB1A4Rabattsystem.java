import java.util.Scanner;

public class AB1A4Rabattsystem {

	public static void main(String[] args) {

		double bestellwert;
		double bestellwertRabatt;
		double bestellwertMwSt;
		double mwst = 0.19;
		double rabatt;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie den Bestellwert ein: ");
		bestellwert = scn.nextDouble();
		
		scn.close();
		
		if (bestellwert > 0 && bestellwert <= 100) {
			rabatt = 0.1;
			bestellwertRabatt = bestellwert - (bestellwert * rabatt);
			bestellwertMwSt = bestellwertRabatt + (bestellwertRabatt * mwst);
		} else if (bestellwert > 100 && bestellwert <= 500) {
			rabatt = 0.15;
			bestellwertRabatt = bestellwert - (bestellwert * rabatt);
			bestellwertMwSt = bestellwertRabatt + (bestellwertRabatt * mwst);
		} else {
			rabatt = 0.2;
			bestellwertRabatt = bestellwert - (bestellwert * rabatt);
			bestellwertMwSt = bestellwertRabatt + (bestellwertRabatt * mwst);
		}
		
		System.out.println("Der erm��igte Bestellwert incl. MwSt lautet: " + bestellwertMwSt + " Euro");
	}

}
