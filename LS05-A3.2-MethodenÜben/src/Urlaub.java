import java.util.Scanner;

public class Urlaub {

	public static void main(String[] args) {
		double ergebnis;
		double waehrung;
		String nameWaehrung;
		String symbolWaehrung;
		double euro;
		
		double budget = getBudget("Bitte geben Sie Ihr Reisebudget ein: ");
		
		int i = 0;
		while (i < 1) {
			if(budget >= 100) {
				String land = getLand("Geben Sie das Gastland/Zielland an: ");
			
				switch(land) {
				case "USA":
					euro = getEuro("Geben Sie den Betrag in Euro ein: ");
					waehrung = 1.22;
					ergebnis = umrechnen(euro,waehrung);
					symbolWaehrung = "USD";
					nameWaehrung = "US Dollar";
					budget = berechneBudget(euro, budget);
					if (budget >= 0) {
						System.out.println(output(euro,ergebnis,symbolWaehrung,nameWaehrung));
						System.out.println(outputBudget(budget));
					}else {
						budget = getBudget("Der von Ihnen gew�hlte Betrag �bersteigt ihr Budget! Bitte geben Sie einen kleineren an: ");
					}
					break;
				case "Japan":
					euro = getEuro("Geben Sie den Betrag in Euro ein: ");
					waehrung = 126.5;
					ergebnis = umrechnen(euro,waehrung);
					symbolWaehrung = "JPY";
					nameWaehrung = "Yen";
					budget = berechneBudget(euro, budget);
					if (budget >= 0) {
						System.out.println(output(euro,ergebnis,symbolWaehrung,nameWaehrung));
						System.out.println(outputBudget(budget));
					}else {
						budget = getBudget("Der von Ihnen gew�hlte Betrag �bersteigt ihr Budget! Bitte geben Sie einen kleineren an: ");
					}
					break;
				case "England":
					euro = getEuro("Geben Sie den Betrag in Euro ein: ");
					waehrung = 0.89;
					ergebnis = umrechnen(euro,waehrung);
					symbolWaehrung = "GBP";
					nameWaehrung = "Pfund";
					budget = berechneBudget(euro, budget);
					if (budget >= 0) {
						System.out.println(output(euro,ergebnis,symbolWaehrung,nameWaehrung));
						System.out.println(outputBudget(budget));
					}else {
						budget = getBudget("Der von Ihnen gew�hlte Betrag �bersteigt ihr Budget! Bitte geben Sie einen kleineren an: ");
					}	
					break;
				case "Schweiz":
					euro = getEuro("Geben Sie den Betrag in Euro ein: ");
					waehrung = 1.08;
					ergebnis = umrechnen(euro,waehrung);
					symbolWaehrung = "CHF";
					nameWaehrung = "Schweizer Franken";
					budget = berechneBudget(euro, budget);
					if (budget >= 0) {
						System.out.println(output(euro,ergebnis,symbolWaehrung,nameWaehrung));
						System.out.println(outputBudget(budget));
					}else {
						budget = getBudget("Der von Ihnen gew�hlte Betrag �bersteigt ihr Budget! Bitte geben Sie einen kleineren an: ");
					}
					break;
				case "Schweden":
					euro = getEuro("Geben Sie den Betrag in Euro ein: ");
					waehrung = 10.10;
					ergebnis = umrechnen(euro,waehrung);
					symbolWaehrung = "SEK";
					nameWaehrung = "Schwedische Kronen";
					budget = berechneBudget(euro, budget);
					if (budget >= 0) {
						System.out.println(output(euro,ergebnis,symbolWaehrung,nameWaehrung));
						System.out.println(outputBudget(budget));
					}else {
						budget = getBudget("Der von Ihnen gew�hlte Betrag �bersteigt ihr Budget! Bitte geben Sie einen kleineren an: ");
					}
					break;
				case "Deutschland":
					System.out.println("Zur�ck in die Heimat!");
					i = 1;
				}
			}else {
				System.out.println("Sie haben Ihre Reserve erreicht bitte begeben Sie sich auf dem Heimweg.");
				i = 1;
			}
		}
	}
	public static String getLand (String aufforderung) {
		Scanner scn = new Scanner(System.in);
		System.out.println(aufforderung);
		String land = scn.next();
		return land;
		}
	public static double getEuro (String aufforderung) {
		Scanner scn = new Scanner(System.in);
		System.out.println(aufforderung);
		double euro = scn.nextDouble();
		return euro;
	}
	public static double umrechnen(double waehrung, double euro) {
		double ergebnis = euro * waehrung;
		return ergebnis;
	}
	public static double getBudget(String aufforderung) {
		Scanner scn = new Scanner(System.in);
		System.out.println(aufforderung);
		double budget = scn.nextDouble();
		return budget;
	}
	public static double berechneBudget(double euro , double budget) {
		budget = budget - euro;
		return budget;
	}
	public static String outputBudget(double budget) {
		String outputBudget = "Ihr verbleibendes Budget lautet: " + budget + "�";
		return outputBudget;
	}
	public static String output(double euro,double ergebnis, String symbolWaehrung, String nameWaehrung) {
		String output = euro + " � sind " + ergebnis + " " + symbolWaehrung + " ("  + nameWaehrung + ")";
		return output;
	}
	
}




