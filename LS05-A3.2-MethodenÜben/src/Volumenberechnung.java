import java.util.Scanner; 

public class Volumenberechnung {

	public static void main(String[] args) {
		
		double a = getWerte("Geben sie a an: ");
		double b = getWerte("Geben sie b an: ");
		double c = getWerte("Geben sie c an: ");
		double h = getWerte("Geben sie h an: ");
		double r = getWerte("Geben sie r an: ");
		
		double ergebnisWuerfel = berechneVolumenWuerfel(a);
			System.out.println("Volumen(Wuerfel)= " + ergebnisWuerfel);
		
		double ergebnisQuader = berechneVolumenQuader(a,b,c);
			System.out.println("Volumen(Quader)=" + ergebnisQuader);
		
		double ergebnisPyramide = berechneVolumenPyramide(a,h);
			System.out.println("Volumen(Pyramide)= "+ ergebnisPyramide);
		
		double ergebnisKugel = berechneVolumenKugel(r);
			System.out.println("Volumen(Kugel)= " + ergebnisKugel);
			
}
	public static double berechneVolumenWuerfel(double a) {
		double ergebnisWuerfel = a*a*a;
		return ergebnisWuerfel;
	}
	public static double berechneVolumenQuader(double a, double b, double c) {
		double ergebnisQuader = a*b*c;
		return ergebnisQuader;
	}
	public static double berechneVolumenPyramide(double a, double h) {
		double ergebnisPyramide = a*a*(h/3);
		return ergebnisPyramide;
	}
	public static double berechneVolumenKugel(double r) {
		double ergebnisKugel = (4/3)*(r*r*r)*3.14;
		return ergebnisKugel;
	}
	
	public static double getWerte(String aufforderung) {
		Scanner scn = new Scanner(System.in);
		System.out.println(aufforderung);
		double wert = scn.nextDouble();
		return wert;
	}

}
