/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 28.09.2021
  * @author << Leon Wolf >>
  */

public class WeltderZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    short anzahlSterne = 400 ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 7670 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000 ;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242 ;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    int flaecheKleinsteLand = 440000 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in Milliarden: " + anzahlSterne);
    
    System.out.println("Einwohnerzahl von Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tieres in Kilogramm: " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Landes in km^2: " + flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Landes in m^2: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
