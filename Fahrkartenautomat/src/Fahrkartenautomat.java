﻿import java.util.Scanner;

public class Fahrkartenautomat

{
	
    public static void main(String[] args)
    {
    	
    	boolean strom = true;
    	
    	while (strom == true) {
    		
    		//Abfragen Einzelpreis und Anzahl der Tickets
    		
    		double betrag = fahrkartenbestellungErfassen("Wählen Sie Ihre Wunschfahrkarte für Berlin aus:");
    		// Geldeinwurf
    		// -----------
    		double rueckgeld = fahrkartenBezahlen(betrag);
    		// Fahrscheinausgabe
    		// -----------------
    		fahrkartenAusgeben();
    		// Rückgeldausgabe
    		// -------------------------------
    		rueckgeldAusgeben(rueckgeld);
    		
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    							"vor Fahrtantritt entwerten zu lassen!\n"+
    							"Wir wünschen Ihnen eine gute Fahrt.");
    		
    		
    		warte (1000);
    		
    		for(int i = 1; i <= 10; i ++) {
    			System.out.println();
    		}
    	}
    }
    
    public static double fahrkartenbestellungErfassen (String aufforderung) {
    	Scanner scn = new Scanner(System.in);
    	int a = 1;
    	double betrag = 0;
    	double zwischensumme = 0;
	
    	while (a == 1) {
    		
        	String[] ticketName = {"(0) Bezahlen","(1) Einzelfahrschein Berlin AB","(2) Einzelfahrschein Berlin BC","(3) Einzelfahrschein Berlin ABC",
        			"(4) Kurzstrecke","(5) Tageskarte Berlin AB","(6) Tageskarte Berlin BC", "(7) Tageskarte Berlin ABC",
        			"(8) Kleingruppe-Tageskarte Berlin AB", "(9) Kleingruppe-Tageskarte Berlin BC", "(10) Kleingruppe-Tageskarte Berlin ABC"};
    		double[] ticketPreis = {zwischensumme, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    		
    		for(int i = 0; i < ticketName.length; i++) {
    			System.out.print(ticketName[i] + " ");
    			System.out.printf("%.2f%s%n",ticketPreis[i], "€");
    		}
    		
    		System.out.println(aufforderung);
    		int ticketTyp = scn.nextInt();
    		double preis = 0;
    		int anzahl = 0;
    		
    		if (ticketTyp == 0) {
    			betrag = zwischensumme;
    			a = 0;
    		} else {
    			anzahl = erfassenAnzahl("Geben Sie die Ticketanzahl ein:");
    			preis = ticketPreis[ticketTyp];
    			zwischensumme = zwischensumme + (preis * anzahl);
    		}
    		betrag = 0;
    		scn.close();
    	}
		return betrag;
    			
    }
    
    public static double fahrkartenBezahlen(double betrag) {
    	Scanner scn = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < betrag)
        {
     	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: " , (betrag - eingezahlterGesamtbetrag)," Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = scn.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        double rueckgeld = eingezahlterGesamtbetrag - betrag;
        scn.close();
    	return rueckgeld;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("%s%.2f%s","Der Rückgabebetrag in Höhe von " , rueckgeld , " EURO ");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2 ," EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1," EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50," CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20," CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10," CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)
            {
            	muenzeAusgeben(5," CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    
    public static void warte(int millisekunde) {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + einheit);
    }
    
    public static int erfassenAnzahl (String aufforderung2) {
    	Scanner scn = new Scanner(System.in);
    	System.out.println(aufforderung2);
    	int anzahl = scn.nextInt();
    	if (anzahl <= 0 || anzahl > 10) {
    		System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
    		anzahl = scn.nextInt();
    		scn.close();
    	}
    	return anzahl;
    }
    
}
