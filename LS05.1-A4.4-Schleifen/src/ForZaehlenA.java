import java.util.Scanner;

public class ForZaehlenA {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie n an:");
		int n = scn.nextInt();
		
		scn.close();
		
		for (int i = 1; i <= n ; i++) {
			System.out.println(i);
		}

	}

}
