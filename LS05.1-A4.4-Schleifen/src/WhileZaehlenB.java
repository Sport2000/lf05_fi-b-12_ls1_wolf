import java.util.Scanner;

public class WhileZaehlenB {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie n ein:");
		int n = scn.nextInt();
		
		scn.close();
		
		int i = n;
		
		while (i > 0) {
			System.out.println(i);
			i--;
		}
	}

}
