import java.util.Scanner;

public class WhileZaehlenA {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie n ein:");
		int n = scn.nextInt();
		
		scn.close();
		
		int i = 1;
		
		while (i <= n) {
			System.out.println(i);
			i++;
		}
	}

}
