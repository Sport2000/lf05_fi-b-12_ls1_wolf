import java.util.Scanner;

public class ForZaehlenB {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie n an:");
		int n = scn.nextInt();
		
		for (int i = n; i >= 1 ; i--) {
			System.out.println(i);
		}
	}
}
