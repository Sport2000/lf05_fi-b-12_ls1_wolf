import java.util.Scanner;

class FahrkartenautomatMethoden

{
    public static void main(String[] args)
    {
       //Abfragen Einzelpreis und Anzahl der Tickets
       double betrag = fahrkartenbestellungErfassen("Geben Sie den Preis des Tickets ein: ", "Geben Sie die Anzahl der Tickets ein: ");
       // Geldeinwurf
       // -----------
       double rueckgeld = fahrkartenBezahlen(betrag);
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       // R�ckgeldausgabe
       // -------------------------------
       rueckgeldAusgeben(rueckgeld);
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    public static double fahrkartenbestellungErfassen (String aufforderung,String aufforderung2) {
    	Scanner scn = new Scanner(System.in);
    	System.out.println(aufforderung);
    	double preis = scn.nextDouble();
    	System.out.println(aufforderung2);
    	int anzahl = scn.nextInt();
    	double betrag = anzahl * preis;
    	return betrag;
    	}
    public static double fahrkartenBezahlen(double betrag) {
    	Scanner scn = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < betrag)
        {
     	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: " , (betrag - eingezahlterGesamtbetrag)," Euro");
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = scn.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        double rueckgeld = eingezahlterGesamtbetrag - betrag;
    	return rueckgeld;
    }
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double rueckgeld) {
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("%s%.2f%s","Der R�ckgabebetrag in H�he von " , rueckgeld , " EURO ");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-M�nzen
            {
            	muenzeAusgeben(2 ," EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1," EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50," CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20," CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10," CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)
            {
            	muenzeAusgeben(5," CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    public static void warte(int millisekunde) {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + einheit);
    }
}