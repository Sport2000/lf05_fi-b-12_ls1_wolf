
public class Ladung {
	
	//Attribute
	private String nameLadung;
	private int anzahlLadung;
	
	//Konstruktoren
	public Ladung() {
		
	}
	
	public Ladung(String nameLadung) {
		this.nameLadung = nameLadung;
	}
	
	public Ladung(String nameLadung, int anzahlLadung) {
		this.nameLadung = nameLadung;
		this.anzahlLadung = anzahlLadung;
	}
	//Methoden
	
	public void setNameLadung(String nameLadung) {
		this.nameLadung = nameLadung;
	}
	public String getNameLadung() {
		return this.nameLadung;
	}
	
	public void setAnzahlLadung(int anzahlLadung) {
		this.anzahlLadung = anzahlLadung;
	}
	public int getAnzahlLadung() {
		return this.anzahlLadung;
	}
}
