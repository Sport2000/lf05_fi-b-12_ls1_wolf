import java.util.ArrayList;


public class Raumschiff
{
	//Attribute
	private String name;
	private double energieversorgungProzent;
	private double schutzschildeProzent;
	private double lebenserhaltungssystemeProzent;
	private double huelleProzent;
	private ArrayList<String> ladungsverzeichnis = new ArrayList<String>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private String torpedoZiel;
	private String phaserZiel;
	
	//Konstruktoren
	public Raumschiff() {
		
	}
	
	public Raumschiff(String name) {
		this.name = name;
	}
	
	public Raumschiff(String name, double energieversorgungProzent, double schutzschildeProzent, double lebenserhaltungssystemeProzent, double huelleProzent) {
		this.name = name;
		this.energieversorgungProzent = energieversorgungProzent;
		this.schutzschildeProzent = schutzschildeProzent;
		this.lebenserhaltungssystemeProzent = lebenserhaltungssystemeProzent;
		this.huelleProzent = huelleProzent;
	}
	
	//Methoden
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	
	public void setEnergieversorgung(double energieversorgungProzent) {
		this.energieversorgungProzent = energieversorgungProzent;
	}
	public double getEnergieversorgung() {
		return this.energieversorgungProzent;
	}
	
	public void setSchutzschilde(double schutzschildeProzent) {
		this.schutzschildeProzent = schutzschildeProzent;
	}
	public double getSchutzschilde() {
		return this.schutzschildeProzent;
	}
	
	public void setLebenserhaltungssysteme(double lebenserhaltungssystemeProzent) {
		this.lebenserhaltungssystemeProzent = lebenserhaltungssystemeProzent;
	}
	public double getLebenserhaltungssysteme() {
		return this.lebenserhaltungssystemeProzent;
	}
	
	public void setHuelle(double huelleProzent) {
		this.huelleProzent = huelleProzent;
	}
	public double getHuelle() {
		return this.huelleProzent;
	}
	
	public void setLadungsverzeichnisNamen(ArrayList<String> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	public ArrayList<String> getLadungsverzeichnisNamen() {
		return this.ladungsverzeichnis;
	}
	
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return this.broadcastKommunikator;
	}
	
	public void setTorpedoZiel(String torpedoZiel) {
		this.torpedoZiel = torpedoZiel;
	}
	public String getTorpedoZiel(){
		return this.torpedoZiel;
	}
	
	public void setPhaserZiel(String phaserZiel) {
		this.phaserZiel = phaserZiel;
	}
	public String getPhaserZiel(){
		return this.phaserZiel;
	}
}
