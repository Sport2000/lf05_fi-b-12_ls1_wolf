
public class Ergebnisse_n {

	public static void main(String[] args) {
		
		//Ergebnis 0!
		System.out.printf("%-5s=%-19s=%4d\n", "0!","", 1);
		//Ergebnis 1!
		System.out.printf("%-5s=%-19s=%4d\n", "1!"," 1", 1);
		//Ergebnis 2!
		System.out.printf("%-5s=%-19s=%4d\n", "2!"," 1 * 2", 2);
		//Ergebnis 3!
		System.out.printf("%-5s=%-19s=%4d\n", "3!"," 1 * 2 * 3", 6);
		//Ergebnis 4!
		System.out.printf("%-5s=%-19s=%4d\n", "4!"," 1 * 2 * 3 * 4", 24);
		//Ergebnis 5!
		System.out.printf("%-5s=%-19s=%4d\n", "5!"," 1 * 2 * 3 * 4 * 5", 120);
	}

}
