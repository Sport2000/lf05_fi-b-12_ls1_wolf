import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
	Scanner myScanner = new Scanner(System.in);
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnisAddition zugewiesen. 
    int ergebnisAddition = zahl1 + zahl2;  
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition); 
    
    //Die Subtraction der Variablen zahl1 und zahl2
    //wird der Variable ergebnisSubtraction zugewiesen.
    int ergebnisSubtraction = zahl1 - zahl2;
    
    System.out.print("\nErgebnis der Subtration lautet: ");
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraction);
    
    //Die Multiplication der Variablen zahl1 und zahl2
    //wird der Variable ergebnisMultiplication zugewiesen.
    int ergebnisMultiplication = zahl1 * zahl2;
    
    System.out.print("\nErgebnis der Multiplication lautet: ");
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplication);
    
    //Die Division der Variablen zahl1 und zahl2
    //wird der Variable ergebnisDivision zugewiesen.
    int ergebnisDivision = zahl1 / zahl2;
    
    System.out.print("\nErgebnis der Division lautet: ");
    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDivision);
    
    
 
    myScanner.close(); 
     
  }    
}