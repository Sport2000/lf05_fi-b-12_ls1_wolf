import java.util.Scanner;

public class Begr��ung {

	public static void main(String[] args) {
		//Begr��ung
		System.out.print("Herzlich Willkommen!");
		
		//Erstellen des Scanner-Objects myScanner
		Scanner myScanner = new Scanner(System.in);
		
		//Abfrage und Speicherung des Namens
		System.out.print("\n\nBitte geben Sie Ihren Vornamen ein: ");
		
		String vornameBenutzer = myScanner.next();
		
		System.out.print("Bitte geben Sie Ihren Nachnamen ein: ");
		
		String nachnameBenutzer = myScanner.next();
		
		//Abfrage und Speicherung des Alters
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
		
		byte alterBenutzer = myScanner.nextByte();
		
		//Ausgabe der Informationen
		
		System.out.println("\n\nInformationen des Benutzers");
		System.out.println("Name: " + vornameBenutzer + " " + nachnameBenutzer);
		System.out.println("Alter: " + alterBenutzer);
		
		myScanner.close();
		
	}

}
