import java.util.Scanner;


public class Addition {
	
	public static void man(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Erste Zahl eingeben: ");
		int zahl01 = scn.nextInt();
		
		System.out.println("Zweite Zahl eingeben: ");
		int zahl02 = scn.nextInt();
		
		int summe = zahl01 + zahl02;
		System.out.println("Summe = " + summe);
		
		scn.close();
	}

}
