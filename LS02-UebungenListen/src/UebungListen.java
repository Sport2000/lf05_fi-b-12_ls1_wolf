
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class UebungListen {

	public static void main(String[] args) {
		int zahl;
		int a;
		int b = 0;
		
		Scanner scn = new Scanner(System.in);
		
		ArrayList<Integer> myList = new ArrayList<Integer>(); //Erstellen des Arrays
		
		for (int i = 0; i <=20; i++) { //Generierung der Zufallszahlen
			int min = 1;
			int max = 10;
			
			Random random = new Random();
			
			a = random.nextInt(max - min) + min; 
			
			myList.add(a); //Hinzuf�gen der Zufallszahl zum array
		}
		
//		for (int i = 1; i <= 9; i++) {
//			for (int j = 1; j <= i; j++) {
//				myList.add(i);
//			}
//		}
//		for (int i = 0; i < myList.size(); i++) {
//		System.out.printf("%s%2d%s%d%n" , "myList[" , i , "]  :  " , myList.get(i) ); //Ausgabe des Arrays
//	}
		
		for (int i = 0; i < 20; i++) {
			System.out.printf("%s%2d%s%d%n" , "myList[" , i , "]  :  " , myList.get(i) ); //Ausgabe des Arrays
		}
		
		System.out.println(""); //Einf�gen einer Leerzeile
		
		System.out.print("Eine Zahl zwischen 1 und 9: "); //Abfrage der Suchzahl
		
		zahl = scn.nextInt();
		
		scn.close();
		
		for (int i = 0; i < 20; i++) { //Abfrage der H�ufigkeit von der Suchzahl
			if (myList.get(i) == zahl) {
				b++;
			} else {
			}
		}
		
		System.out.println(""); //Einf�gen einer Leerzeile
		
		System.out.printf("%s%d%s%d%s%n%n", "Die Zahl ", zahl , " kommt ", b , " mal in der Liste vor."); //Ausgabe der H�ufigkeit von Suchzahl
		
		System.out.println("Die Zahl komm an folgenden Indices in der Liste vor:");
		
		for (int i = 0; i < 20; i++) { //Abfrage des Vorkommen von der Suchzahl
			if (myList.get(i) == zahl) {
				System.out.printf("%s%2d%s%d%n" , "myList[" , i , "]  :  " , myList.get(i) );
			} else {
			}
		}
		
		System.out.println(""); //Einf�gen einer Leerzeile
		
		System.out.println("Liste nach L�schung von " + zahl + ":");
		
		for (int j = 0; j <= 5; j++) { //wiederholung der L�schung um Problem in *1 zu umgehen
			for (int i = 0; i < myList.size(); i++) { //L�schen der Indices  
				if (myList.get(i) == zahl) {
					myList.remove(i);
				} else {
				}
			}
		}
		
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("%s%2d%s%d%n" , "myList[" , i , "]  :  " , myList.get(i) ); //Ausgabe des Arrays
		}
		
		System.out.println(""); //Einf�gen einer Leerzeile
		
		System.out.println("Liste nach Einf�gen von 0 hinter jeder 5:");	
		
		for (int i = 0; i < myList.size(); i++) { //Einf�gen von 0 hinter jeder 5 
			if (myList.get(i) == 5) {
				myList.add(i + 1, 0);
			} else {
			}
		}
		
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("%s%2d%s%d%n" , "myList[" , i , "]  :  " , myList.get(i) ); //Ausgabe des Arrays
		}
	}
}

//*1 Problem bei der L�schung von Indices: 
//Wenn mehrere aufeinander folgende Indices die Suchzahl haben wird beim einmaligen L�schen nur der erste Index mit der Suchzahl gel�scht.
//Dies kann durch wiederholen des L�schvorgangs gel�st werden, allerdings kommt der Fehler auch vor, wenn die Suchzahl 4 mal hintereinander vorkommt 
//und nur 2-mal gel�scht wird.