import java.util.Scanner;

class Fahrkartenautomat2
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       int anzahlKarten;
       float einzelpreis;
       
       //Abfragen Einzelpreis und Anzahl der Tickets
       
       System.out.print("Ticket Einzelpreis (EURO): ");
       einzelpreis = tastatur.nextFloat();
       System.out.print("Anzahl der Tickets: ");
       anzahlKarten = tastatur.nextInt();
       
       //Berechnung des Gesamtpreises
       
       zuZahlenderBetrag = anzahlKarten * einzelpreis;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag)," Euro");
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("%s%.2f%s","Der R�ckgabebetrag in H�he von " , r�ckgabebetrag , " EURO ");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       tastatur.close();
    }
}
//1)	Die im Quelltext erstellten Variablen zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneM�nze, r�ckgabebetrag 
//		sind alle vom Datentyp double
//2)	Die mit den Variablen durchgef�hrten Operationen sind: Gr��er gleich; Substraktionszuweisung und Produkt 
//5) 	Der Datentyp Int wurde f�r die Anzahl der Tickets gew�hlt, da man nur ganze Tickets kaufen kann also nur ganze Zahlen benutzt werden
//		Der Datentyp float wurde f�r die einzelpreis gew�hlt, da es nur zwei Nachkommastellen wichtig sind
//6)	Bei der Berechnung von anzahlKarten * einzelpreis, guckt das Programm erst welche Werte den Variablen gegeben wurde,
//		danach wird anzahlKarten mit den einzelpreis multipliziert. 
//		Das Ergebnis wird nun der Variable zuZahlenderBetrag zugeordnet.