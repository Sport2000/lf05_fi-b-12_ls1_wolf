import java.util.Scanner;

public class Fahrkartenautomat

{
    public static void main(String[] args)
    {
    
    	boolean strom = true;
    	
    	while (strom == true) {
    		
    		//Abfragen Einzelpreis und Anzahl der Tickets
    		double betrag = fahrkartenbestellungErfassen("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: \n  "
    				+ "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n  "
    				+ "Tageskarte Regeltarif AB [8,60 EUR] (2)\n  "
    				+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"
    				, "Geben Sie die Anzahl der Tickets ein: ");
    		// Geldeinwurf
    		// -----------
    		double rueckgeld = fahrkartenBezahlen(betrag);
    		// Fahrscheinausgabe
    		// -----------------
    		fahrkartenAusgeben();
    		// R�ckgeldausgabe
    		// -------------------------------
    		rueckgeldAusgeben(rueckgeld);
    		
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    							"vor Fahrtantritt entwerten zu lassen!\n"+
    							"Wir w�nschen Ihnen eine gute Fahrt.");
    		
    		
    		warte (1000);
    		
    		for(int i = 1; i <= 10; i ++) {
    			System.out.println();
    		}
    	}
    }
    
    public static double fahrkartenbestellungErfassen (String aufforderung,String aufforderung2) {
    	Scanner scn = new Scanner(System.in);
    	
    	System.out.println(aufforderung);
    	int ticketTyp = scn.nextInt();
    	double preis = 0;
    	
    	while (preis == 0) {
    		if (ticketTyp == 1) {
    			preis = 2.90;
    		} else if (ticketTyp == 2) {
    			preis = 8.60;
    		} else if (ticketTyp == 3) {
    			preis = 23.50;
    		} else {
    			System.out.println("Eingabe Ung�ltig!");
    		}
    	}
    	
    	
    	System.out.println(aufforderung2);
    	int anzahl = scn.nextInt();
    	if (anzahl < 0 || anzahl > 10) {
    		anzahl = 1;
    		System.out.println("Ihre Eingabe ist ung�ltig! 1 Ticket wird gekauft.");
    	}
    	double betrag = anzahl * preis;
    	return betrag; 
    }
    
    public static double fahrkartenBezahlen(double betrag) {
    	Scanner scn = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < betrag)
        {
     	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: " , (betrag - eingezahlterGesamtbetrag)," Euro");
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = scn.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        double rueckgeld = eingezahlterGesamtbetrag - betrag;
    	return rueckgeld;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("%s%.2f%s","Der R�ckgabebetrag in H�he von " , rueckgeld , " EURO ");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-M�nzen
            {
            	muenzeAusgeben(2 ," EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1," EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50," CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20," CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10," CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)
            {
            	muenzeAusgeben(5," CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    
    public static void warte(int millisekunde) {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + einheit);
    }
}